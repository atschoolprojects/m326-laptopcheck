# LaptopCheck

## About
This Project was created by Manuel Gwerder and Mauro Mori.

We got this order during our regular school classes in M326.

The order is to create an application which can rate laptops from new students according to the requirements from their vocation.
The application can rate laptops from five different vocations and  gives each of them an individual grade from 1-6 where 6 is the highest possible score. The appplication does not only show the requirements but also some recommendations, so that the student knows what would be the best possible laptop to have during their apprenticeship.


## Environment 
This Project was made with Angular, because both of us already came across this language and we both thought that Angular is an interesting and simple language.

### Set up Environment
First you need to install Angular if you haven't already.

Afterwards you can clone the repository and open it with your favourite Editor.

Open a terminal and run the command `npm install`.

Afterwards type the command `ng serve --open` and wait until the Browser opens automatically.
