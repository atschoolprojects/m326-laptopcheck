import { Component, OnInit } from '@angular/core';
import { DevicespecService } from '../../Services/devicespec/devicespec.service';
import { Vocation } from '../../Models/vocation';
import { StorageType } from 'src/app/Models/storageType';
import { FormBuilder, Validators } from '@angular/forms';
import { DevicecomponentService } from 'src/app/Services/devicecomponent/devicecomponent.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-data-check',
  templateUrl: './data-check.component.html',
  styleUrls: ['./data-check.component.css']
})
export class DataCheckComponent implements OnInit {

  gridCols: number;
  Vocation = Vocation;
  StorageType = StorageType;

  vocationForm = this.formBuilder.group(
    {
      vocation: ['', Validators.required],
    }
  );

  componentsForm = this.formBuilder.group(
    {
      core_count: [],
      frequency: [],
      memory: [],
      capacity: [],
      display_size: [],
      storage_type: [],
    }
  );

  toggleForm = this.formBuilder.group(
    {
      touchscreen: [false, Validators.required],
      pen: [false, Validators.required],
      usb_c: [false, Validators.required],
      ethernet: [false, Validators.required]
    }
  );

  constructor(
    public devicespecService: DevicespecService,
    public devicecomponentService: DevicecomponentService,
    public router: Router,
    private formBuilder: FormBuilder
  ) { }


  ngOnInit(): void {
    this.calculateCols(window.innerWidth);
  }

  resize(event) {
    this.calculateCols(event.target.innerWidth);
  }

  calculateCols(newWidth: any) {
    this.gridCols = (newWidth <= 800) ? 1 : 2; // smaller than 800 px -> 1 col else 2 cols
  }
/**
 * Validates user input and edit it if necessary
 * @param validatorName The name this inputfield has in the formGroup
 * @param possibleValues Every value that is valid for this inputfield
 */
  validateUserInput(validatorName: string, possibleValues: number[]) {
    const validator = this.componentsForm.get(validatorName);
    const input = validator.value;
    const firstElement = possibleValues[0];
    const lastElement = possibleValues[possibleValues.length - 1];
    
    if (typeof input === 'number') { // because leaving the field without an input fires event it has to be filtered
      if (!possibleValues.includes(input)) { // does input exists in the possibleValues
        if (input >= firstElement) { // is the input bigger than the lowest possibility?
          if (input <= lastElement) { // is the input smaller than the highest possibility?
           for (const possibleValue of possibleValues) { // foreach possibility
              const lastPossibleValue = possibleValues[possibleValues.indexOf(possibleValue) - 1];
              
              if (input > lastPossibleValue && input < possibleValue) { // is the input between the last possibility and the current one?
                validator.setValue(lastPossibleValue); // sets the last possibility as the users input
                break;  // leave the loop
              }
            }
          } else {
            // input higher than possible
            validator.setValue(lastElement); // sets the last possibleValue as userinput
          }
        } else {
          // input lower than possible
          validator.setValue(firstElement); // sets the lowest possibleValue as userinput
        }
      }
    }
  }

  sendData() {
    const vocationF = this.vocationForm.controls;
    const componentsF = this.componentsForm.controls;
    const toggleF = this.toggleForm.controls;

    this.devicecomponentService.vocationId = vocationF.vocation.value;

    this.devicecomponentService.deviceComponents = {
      processor_core_count: Number(componentsF.core_count.value),
      processor_frequency_ghz: Number(componentsF.frequency.value),
      memory_gb: Number(componentsF.memory.value),
      storage_capacity_gb: Number(componentsF.capacity.value),
      display_size_inch: Number(componentsF.display_size.value),
      storage_type: componentsF.storage_type.value,
      touchscreen: toggleF.touchscreen.value,
      pen: toggleF.pen.value,
      usb_c: toggleF.usb_c.value,
      ethernet: toggleF.ethernet.value
    };
    this.router.navigate(['/rating']);
  }

}

