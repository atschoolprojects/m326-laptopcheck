import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { VocationPipe } from 'src/app/Pipes/vocation/vocation.pipe';
import { DeviceComponentMock } from 'src/app/Services/devicecomponent/devicecomponent-mock.service';
import { DeviceSpecMock } from 'src/app/Services/devicespec/devicespec-mock.service';
import { RatingComponent } from './rating.component';


describe('RatingComponent', () => {
  let component: RatingComponent;
  let deviceComponentService: DeviceComponentMock;
  let deviceSpecsService: DeviceSpecMock;
  let fixture: ComponentFixture<RatingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        BrowserAnimationsModule
      ],
      declarations: [ 
        RatingComponent,
        VocationPipe
      ],
      providers: [
        { provide: DeviceComponentMock, useClass: DeviceComponentMock },
        { provide: DeviceSpecMock, useClass: DeviceSpecMock}
      ],

    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RatingComponent);
    component = fixture.componentInstance;
    deviceComponentService = TestBed.get(DeviceComponentMock);
    deviceSpecsService = TestBed.get(DeviceSpecMock);
    fixture.detectChanges();
  });

  function initialize() {
    component.deviceComponents = deviceComponentService.deviceComponents;
    component.devicespecs = deviceSpecsService.deviceSpecs;
  }

  it('T16: Should calculate ratingIndex (number)', () => {
    initialize();
    expect(component.getRatingIndex(['display_size_inch', 17.3])).toBe(0); 
  });
  
  it('T17: Should calculate ratingIndex (string)', () => {
    initialize();
    expect(component.getRatingIndex(['storage_type', 2])).toBe(0);
  });

  it('T18: Should calculate ratingIndex (boolean)', () => {
    initialize();
    expect(component.getRatingIndex(['ethernet', false])).toBe(2);
  });

  it('T19: Should return index for unknown', () => {
    initialize();
    expect(component.getRatingIndex(['pen', {}])).toBe(3);
  });

  it('T20: Should get required data for diagram', () => {
    initialize();
    component.createDiagramArrays();
    expect(component.diagramDataReq).toEqual([2, 2, 4, 200, 13, 1, 1, 1, 1, 2]);
  });

  it('T21: Should get recommended data for diagram', () => {
    initialize();
    component.createDiagramArrays();
    expect(component.diagramDataRec).toEqual([4, 2.2, 8, 250, 15, 2, 2, 2, 2, 2]);
  });

  it('T22: Should inverts the animation variable', () => {
    component.showDetails = false;
    component.triggerAnimation();
    expect(component.showDetails).toBe(true);
  });

  it('T23: Should inverts the diagram variable', () => {
    component.showDiagram = false;
    component.changeView();
    expect(component.showDiagram).toBe(true);
  });
});
