import { Injectable } from '@angular/core';
import { DeviceComponents } from 'src/app/Models/DeviceComponents';

@Injectable({
  providedIn: 'root'
})
export class DevicecomponentService {

  private vocation = '-1';
  private components: DeviceComponents = { };

  constructor() { }

  get vocationId() {
    return this.vocation;
  }

  set vocationId(vocationId: string) {
    this.vocation = vocationId;
  }

  get deviceComponents() {
    return this.components;
  }

  set deviceComponents(deviceComponents: DeviceComponents) {
    this.components = deviceComponents;
  }
}
