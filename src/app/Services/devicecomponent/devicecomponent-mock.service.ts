import { StorageType } from "src/app/Models/storageType";

export class DeviceComponentMock {

    get vocationId() {
        return '45';
    }

    get deviceComponents() {
        return {
            processor_core_count: 6,
            processor_frequency_ghz: 2,
            memory_gb: 8,
            storage_capacity_gb: 512,
            display_size_inch: 17.3,
            storage_type: StorageType.SSD,
            touchscreen: false,
            pen: false,
            usb_c: true,
            ethernet: false
        }
    }
  }