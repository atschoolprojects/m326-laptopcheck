import { Routes } from '@angular/router';
import { DataCheckComponent } from './Views/data-check/data-check.component';
import { RatingComponent } from './Views/rating/rating.component';

export const appRoutes: Routes = [
  { path: 'data-check', component: DataCheckComponent },
  { path: 'rating', component: RatingComponent},
  { path: '', redirectTo: 'data-check', pathMatch: 'full' }
];
