import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgxEchartsModule } from 'ngx-echarts';
import { AppComponent } from './app.component';
import { appRoutes } from './app.routes';
import { ToolbarComponent } from './Components/toolbar/toolbar.component';
import { DimensionPipe } from './Pipes/dimension/dimension.pipe';
import { DisplayFormatPipe } from './Pipes/displayFormat/displayFormat.pipe';
import { VocationPipe } from './Pipes/vocation/vocation.pipe';
import { DataCheckComponent } from './Views/data-check/data-check.component';
import { RatingComponent } from './Views/rating/rating.component';



@NgModule({
  declarations: [
    AppComponent,
    DataCheckComponent,
    RatingComponent,
    ToolbarComponent,
    DisplayFormatPipe,
    VocationPipe,
    DimensionPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts'),
    }),
    FormsModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatToolbarModule,
    MatTableModule,
    MatSelectModule,
    MatCardModule,
    MatIconModule,
    MatInputModule,
    MatSlideToggleModule,
    MatGridListModule,
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
