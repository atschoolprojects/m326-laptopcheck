import { DimensionPipe } from './dimension.pipe';

describe('DimensionPipe', () => {
  it('T3: Should translate display size', () => {
    const pipe = new DimensionPipe();
    expect(pipe.transform('display_size_inch')).toBe('Bildschirmgrösse (Zoll)');
  });

  it('T4: Shlould retrun an Error', () => {
    const pipe = new DimensionPipe();
    expect(pipe.transform('no_diemsion')).toBe('Error');
  })
});
