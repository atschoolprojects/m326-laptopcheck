import { VocationPipe } from './vocation.pipe';

describe('VocationPipe', () => {
  it('T10: Should convert vocation', () => {
    const pipe = new VocationPipe();
    expect(pipe.transform('22')).toBe('Koch / Köchin EFZ');
  });

  it('T11: Should return Error', () => {
    const pipe = new VocationPipe();
    expect(pipe.transform('-1')).toBe('Error');
  });
});
