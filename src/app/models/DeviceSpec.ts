import { DeviceComponents } from './DeviceComponents';

export interface DeviceSpec {
    vocation_id: string;
    vocation_title: string;
    spec: {
        required: DeviceComponents;
        recommended: DeviceComponents;
    };
}
