export interface TableElement {
    dimension: string;
    deviceComponent: string;
    result: number;
}
