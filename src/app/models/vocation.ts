export enum Vocation {
    Informatiker = '45',
    Schreiner = '48',
    Hauswirtschaftpraktiker = '55',
    Zeichner = '37',
    Koch = '22'
}
